# How to
1) Install [docker-engine](https://docs.docker.com/engine/install/ubuntu/)
   (**ON LINUX DO NOT INSTALL DOCKER DESKTOP**), 
   do [post-installation steps](https://docs.docker.com/engine/install/linux-postinstall/).
   - For Ubuntu (and Mint) users:  

       - if you are a mint user, change VERSION_CODENAME to UBUNTU_CODENAME  
    
       ```
       sudo apt-get update
       sudo apt-get install ca-certificates curl gnupg
       sudo install -m 0755 -d /etc/apt/keyrings
       curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
       sudo chmod a+r /etc/apt/keyrings/docker.gpg

       echo \
         "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
         "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
         sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
       sudo apt-get update
       sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
       ```  
    
       - and post-installation to use docker without sudo:
    
       ```
       sudo groupadd docker
       sudo usermod -aG docker $USER
       ```
       - and restart your computer
2) Clone this repository
  - `SOME_PATH` here stands for any path on your computer where you want to have the files
   ```
   cd SOME_PATH
   git clone https://gitlab.fel.cvut.cz/body-schema/teaching/b3m33hro-labs.git  
   ```  

3) Pull or build the image  

4) Use it  
  - On Linux (e.g, Ubuntu) everything should work fine directly in your environment as usually
  - **On Windows and Mac, you need to use VNC to see the GUI applications**
    - you have to run the container with `-vnc` (or run `vnc` in the container) and then run `start-vnc-session.sh` 
      in the container
      - now you can open http://localhost:6080 in your browser to see the environment

## Pulling the image from Docker Hub 
- go to Docker directory  
    `cd SOME_PATH/b3m33hro-labs/Docker`
- and run (Linux only)  
    `./deploy.py -c CONTAINER_NAME -p PATH_TO_DATA -pu`
  - where `CONTAINER_NAME` is the name of the container you want to create (e.g., hro) and `PATH_TO_DATA` is the path 
    to folder you want to see in the Docker (e.g., SOME_PATH)
  - On Windows and Mac run `./deploy.py -c CONTAINER_NAME -p PATH_TO_DATA -pu -vnc`

## Building the image from Dockerfile
- this options is suitable when you have problems with pulling or when you run into problems from [FAQ](#faq)
- the steps are similar to [Pulling the image](#pulling-the-image-from-docker-hub) you need to replace `-pu` with `-b` in the command
- also, you need to download [libsoqt.zip](https://drive.google.com/file/d/1A5jneVmrLIbx22gRiTC0SB7M4H1WtJYs/view?usp=sharing)
  and extract it to `SOME_PATH/b3m33hro-labs/Docker/additional_files/libsoqt` directory

## Working with docker
- after you pull or build the image and run the container (we assume you called the container `hro`) you can:
  - start the container again
    - `./deploy.py -c hro -e`
    - `-e` stands for 'existing'
  - run the container with another path visible to docker
    - `./deploy.py -c hro -p SOME_NEW_PATH`
  - run it with VNC settings
    - `./deploy.py -c hro -e -vnc`
    - `-vnc` will set DISPLAY variable to 99 to make VNC work
  - open new terminal in existing container
    - `./deploy.py -c hro -t`

# FAQ
### FAQ
  - **you get error of not being in sudo group when running image**
    - check output of `id -u` command. If the output is not 1000 you have to build the image
      by yourself and can not pull it
      - this happens when your account is not the first one created on your computer
  - **`sudo apt install something` does not work**
    - you need to run `sudo apt update` first after you run the container for the first time
      - apt things are removed in Dockerfile, so it does not take unnecessary space in the image
  - **the VNC stop working after some time**
    - run `start-vnc-session.sh` again